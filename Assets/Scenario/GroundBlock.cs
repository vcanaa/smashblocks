﻿using System.Collections.Generic;
using UnityEngine;


public class GroundBlock : Block {
    public Transform spriteTransform;

    public float velToCatch;

    public int spawnerWidth;
    public int spawnerHeight;

    public bool breakable;

    public GameObject explosion;

    bool foo;
    protected override void Awake() {
        //Debug.Log("" + colliders.Length);
        base.Awake();
        body.isKinematic = true;
        boxCollider.sharedMaterial.friction = 0.5f;
    }
    protected override void Start() {
        base.Start();
    }

    public override void Update() {
        base.Update();
        //if (grabbedBy == null && body.velocity.magnitude < 0.01) {
        //    body.isKinematic = true;
        //}

        //body.isKinematic = true;
        if (this.transform.position.y < -30 && foo) {
            DestroyGameObject();
        }
    }

    public override void HitPlayer(PlayerControl player) {
        if (!breakable) return;
        float x = 3;
        Explode(prevVel * 0.1f, x, x);
    }

    public void Explode(Vector2 vel, float x, float y) {
        if (!breakable) return;
        Disconnect();
        this.EnableAllColliders(false);
        GameObject.Instantiate(explosion, transform.position, Quaternion.identity);
        //CreateFragment(new Vector2(x, y) + vel);
        //CreateFragment(new Vector2(-x, y) + vel);
        //CreateFragment(new Vector2(x, 0) + vel);
        //CreateFragment(new Vector2(-x, 0) + vel);

        this.gameObject.SetActive(false);
    }

    private void CreateFragment(Vector2 vel) {
        var b = Copy() as GroundBlock;
        b.foo = true;
        b.grabbedBy = null;
        b.harmful = false;
        b.body.velocity = vel;
        b.transform.localScale = b.transform.localScale * 0.5f;
    }
}
