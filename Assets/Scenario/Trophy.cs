﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Trophy : MonoBehaviour {
    public Timeout fallTimeout;

    void Start() {
        fallTimeout.callback = Enable;
        fallTimeout.Start();
    }

    void Update() {
        fallTimeout.Update(Time.deltaTime);
    }

    void Enable() {
        var groundBlock = this.GetComponent<GroundBlock>();
        groundBlock.enabled = true;
        var body = this.GetComponent<Rigidbody2D>();
        body.isKinematic = false;
    }
}
