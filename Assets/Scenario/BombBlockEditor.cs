﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BombBlock))]
public class BombBlockEditor : BlockEditor {

    protected override void OnSceneGui(SceneView view) {
        BombBlock t = (BombBlock) target;
        Handles.DrawWireDisc(t.transform.position, Vector3.forward, t.radius);

        Snap();
    }
}
