﻿using System.Collections.Generic;
using UnityEngine;


public class BombBlock : Block {
    public GameObject explosion;

    public ExplosionArea explosionArea;
    public float bombPower;

    public Timeout timer;

    public Timeout blinkTimer;
    public float blinkMax;
    public float blinkMin;
    public float blinkDecay;
    int blinkCount;

    public SpriteRenderer sprite;
    public float radius;

    protected override void Awake() {
        base.Awake();
        UseBoxCollider(true);

        timer.callback = Explode;

        blinkTimer.callback = () => {
            if (blinkCount % 2 == 0) {
                sprite.color = Color.red;
            } else {
                sprite.color = Color.white;
            }
            blinkCount++;

            if (blinkCount == 1) {
                blinkTimer.total = blinkMax;
            }

            blinkTimer.total -= blinkDecay;
            if (blinkTimer.total < blinkMin) {
                blinkTimer.total = blinkMin;
            }

            blinkTimer.Start();
        };
    }

    protected override void Start() {
        base.Start();
        timer.Start();
        blinkTimer.Start();
    }

    public override void Update() {
        base.Update();
        timer.Update(Time.deltaTime);
        blinkTimer.Update(Time.deltaTime);
    }

    //public override void HitPlayer(PlayerControl player) {
    //    Explode();
    //}

    public void Explode() {
        var expl = GameObject.Instantiate(explosion, transform.position, Quaternion.identity);
        foreach (Collider2D col in Physics2D.OverlapCircleAll(transform.position, 2)) {
            HandleExplosion(col);
        }

        DestroyGameObject();
    }

    void HandleExplosion(Collider2D col) {
        PlayerControl player = col.gameObject.GetComponent<PlayerControl>();
        if (player != null) {
            //aDebug.Log("Hit player");
            Vector2 diff = player.transform.position - transform.position;
            diff.Normalize();
            player.body.velocity = diff * bombPower;
            player.hitTimeout.Start();
            return;
        }

        GroundBlock ground = col.gameObject.GetComponent<GroundBlock>();
        if (ground != null) {
            Vector2 diff = ground.transform.position - transform.position;
            diff.Normalize();
            diff *= bombPower;
            ground.Explode(diff/2, 3, 3);
            return;
        }
    }
}
