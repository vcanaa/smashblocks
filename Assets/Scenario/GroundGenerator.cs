﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GroundGenerator : MonoBehaviour {

    public Vector3 lookRotation;

    public Transform environment;

    public Material material;

    public float motionMaxSpeed;
    public float motionThreshold;
    public float motionAbruptness;
    public float groundlevel;
    public float overGroundLevel;

    List<MovingGround> movingGrounds;

    public int nLayers;
    public Vector2 startPosition;
    private Vector2 _startPosition;
    public Vector2 fullSize;
    private Vector2 _fullSize;

    public float maxHeight;
    public float minHeight;

    public bool initLayers;

    void Start() {
        movingGrounds = new List<MovingGround>();
        initLayers = true;
    }

    void Update() {
        // Handle variable changes in editor
        if (fullSize != _fullSize ||
            startPosition != _startPosition) {
            initLayers = true;
        }

        if (movingGrounds.Count != nLayers) {
            for (int i = 0; i < movingGrounds.Count; i++) {
                DestroyObject(movingGrounds[i].gameObject);
            }
            movingGrounds.Clear();
            for (int i = 0; i < nLayers; i++) {
                movingGrounds.Add(CreateLayer());
            }
            initLayers = true;
        }

        if (initLayers) {
            //aDebug.Log("InitLayers");
            for(int i = 0; i < movingGrounds.Count; i++) {
                InitLayer(movingGrounds[i], i);
            }
            _fullSize = fullSize;
            _startPosition = startPosition;
            initLayers = false;
        }
    }

    public void ElevateGround(float x, float spread, float dY) {
        int layer1 = (int)((x - startPosition.x - spread / 2) / fullSize.x * nLayers);
        int layer2 = (int)((x - startPosition.x + spread / 2) / fullSize.x * nLayers);

        ////aDebug.Log("Elevate " + layer1);
        for (int layer = layer1; layer <= layer2; layer++) { 
            if (layer > 0 && layer < nLayers) {
                movingGrounds[layer].ChangeEndHeight(dY);
            } 
        }
    }

    public void SetGroundHeight(float x, float spread, float height) {
        if (height > maxHeight) {
            height = maxHeight;
        }

        if (height < minHeight) {
            height = minHeight;
        }

        int layer1 = (int)((x - startPosition.x - spread / 2) / fullSize.x * nLayers);
        int layer2 = (int)((x - startPosition.x + spread / 2) / fullSize.x * nLayers);

        ////aDebug.Log("Elevate " + layer1);
        for (int layer = layer1; layer <= layer2; layer++) {
            if (layer > 0 && layer < nLayers) {
                movingGrounds[layer].SetEndHeight(height - fullSize.y / 2);
            }
        }
    }

    private void InitLayer(MovingGround movingGround, int layer) {
        movingGround.Init(
            new Vector2(startPosition.x + layer * fullSize.x / nLayers, startPosition.y - fullSize.y / 2),
            new Vector2(fullSize.x / nLayers, fullSize.y));
    }

    private MovingGround CreateLayer()
    {
        GameObject obj = new GameObject("PieceOfGround");
        MovingGround movingGround = obj.AddComponent<MovingGround>();
        movingGround.groundGenerator = this;
        movingGround.startPos = new Vector2(0, startPosition.y);
        movingGround.endPos = movingGround.startPos;
        return movingGround;
    }
}
