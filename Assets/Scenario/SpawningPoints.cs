﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SpawningPoints : MonoBehaviour {
    public List<Transform> points;

    void Awake() {
        if (points.Count == 0) {
            foreach (Transform c in transform) {
                points.Add(c);
            }
        }
    }

    public Vector3 GetPos(int i) {
        return points[i].transform.position;
    }
}
