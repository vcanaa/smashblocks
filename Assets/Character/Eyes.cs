﻿using System.Collections;
using UnityEngine;

public class Eyes : MonoBehaviour {
    public GameObject eyeL;
    public GameObject eyeR;

    public SpriteRenderer eyeLSprite;
    public SpriteRenderer eyeRSprite;

    public Animator animator;
    string lastAnimation;

    public float blinkInterval;

    void Start() {
        Vector3 s = eyeL.transform.localScale;
        s.x = -s.x;
        eyeR.transform.localScale = s;

        eyeLSprite = eyeL.GetComponent<SpriteRenderer>();
        eyeRSprite = eyeR.GetComponent<SpriteRenderer>();

        StartCoroutine(UpdateInternal());
    }

    IEnumerator UpdateInternal() {
        while (true) {
            //aDebug.Log("Test blink " + lastAnimation);
            if (lastAnimation == "open" || lastAnimation == "blink") {
                Blink();
            }

            yield return new WaitForSeconds(UnityEngine.Random.Range(blinkInterval / 2, blinkInterval));
        }
    }

    public void Blink() {
        Play("blink");
    }

    public void Close() {
        Play("close");
    }

    public void Pain() {
        Play("pain");
    }

    public void Open() {
        Play("open");
    }

    void Play(string animation) {
        //aDebug.Log("eye" + animation);
        animator.Play(animation);
        lastAnimation = animation;
    }
}