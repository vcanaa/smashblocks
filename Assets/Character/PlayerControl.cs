﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class PlayerControl : Block {

    // State variables
    public bool standing;
    public bool running;
    public bool holdingWall;
    public Vector2 facingDir;
    public bool onWall;
    public Vector2 dir1Raw;
    public Vector2 dir1;

    public int remainingAirJumps;
    public int remainingDashes;

    public GameObject bodyObject;
    public Eyes eyes;
    public SpriteRenderer crown;

    public List<Sprite> sprites;

    public ControllerWrapperBase controller;
    //List<Collision2D> collisionEnter;
    //List<Collision2D> collisionStay;

    Timeout dashTimeout;
    //Timeout reDashTimeout;
    public Timeout hitTimeout;
    Timeout standingTimeout;
    Timeout holdingWallTimeout;
    public Timeout frozenTimeout;

    BasicPlayerConfig c;
    Vector2 dashDir = Vector2.zero;

    public bool crouching;
    public bool alive;

    public AppearAnimation appearAnimation;
    public AppearAnimation disappearAnimation;

    public float hitMultiplier;

    Vector3 hitImpact = Vector3.zero;

    Block grabbedBlock;

    public bool falling;
    public float rotation;
    public float rotationSpeed;
    public float fallingSpeed;
    public float fallingScale;

    public List<SpriteRenderer> headItems;
    public List<SpriteRenderer> eyeItems;
    public List<SpriteRenderer> mouthItems;

    Vector2 normalSumUnit = Vector2.zero;
    Vector2 normalXSumUnit = Vector2.zero;
    Vector2 normalYSumUnit = Vector2.zero;
    List<ContactPoint2D> hContP = new List<ContactPoint2D>();

    Block bestBlockToGrab;
    float bestBlockToGrabDistance;

    public bool doingStep = false;

    public float targetScaleY;
    public float scaleY;
    public float dScaleY;
    public float dScaleYMax;
    public float dScaleYThr;
    public float scaleYOmega;
    public float scaleYBounce;
    public float scaleYDecay;
    public float normalScale;

    Vector2 initialSize;

    public bool explodingBlocks;

    public float maxStandingFriction;
    public float maxWalFriction;

    public ParticleSystem dashEffect;
    public ParticleSystem hitTrail;

    float jumpLag = 0.1f;

    protected override void Awake() {
        base.Awake();

        EntityHub.Untag(this, Tags.SceneItem);
        UseBoxCollider(true);

        c = BasicPlayerConfig.get;
        //collisionEnter = new List<Collision2D>();
        //collisionStay = new List<Collision2D>();
        facingDir = new Vector2(1, 0);

        dashTimeout = new Timeout {
            callback = () => { explodingBlocks = false; },
            cancelCallback = () => { explodingBlocks = false; },
            total = c.dashTime
        };

        //reDashTimeout = new Timeout {
        //    total = c.redashTime
        //};

        hitTimeout = new Timeout {
            total = c.hitTime,
            callback = () => {
                this.EnableAllColliders(true);
                eyes.Open();
            }
        };

        standingTimeout = new Timeout {
            total = c.standingCooldownLimit
        };

        holdingWallTimeout = new Timeout {
            total = c.holdingWallCooldownLimit
        };

        scaleY = 1;
        initialSize = this.transform.localScale;
    }

    ControllerWrapperBase Controller {
        get {
            if (controller == null) {
                controller = this.GetComponent<ControllerWrapperBase>();
            }

            return controller;
        }
    }

    public override void Update() {
        if (!hitTrail.isStopped && (body.velocity.magnitude < 0.01f)) {
            hitTrail.Stop();
        }

        if (falling) {
            FallOnBackground();
            return;
        }

        if (!alive) return;
        frozenTimeout.Update(Time.deltaTime);

        Vector2 vel = body.velocity;

        if (frozenTimeout.on) {
            Controller.enabled = false;
        } else {
            Controller.enabled = true;
        }

        NormalizeNormals();

        UpdateTimeouts();

        if (grabbedBy != null) {
            hitTimeout.Start();
        }

        CalculateGrabBlock(bestBlockToGrab);

        // Throw block
        if (Controller.GetKeyUp("X")) {
            ThrowBlock();
        }

        if (standingTimeout.on) {
            standing = true;
        }

        if (hitTimeout.on) {
            eyes.Pain();
        }

        CalculateDirections();

        crouching = dir1.y <= c.crouchLimit;

        vel = CalculateStepOver(vel);

        vel = CalculateDash(vel);

        vel = CalculateAirJump(vel);

        ResetWallJumpAndDash();

        vel = CalculateJump(vel);

        vel = CalculateRun(vel);

        vel = CalculateIdle(vel);

        CalculateOnWall();

        vel = CalculateAirControl(vel);

        vel = CalculateMaxFall(vel);

        body.velocity = vel;

        CalculateScale();

        Controller.UpdateKeySet(Time.deltaTime);

        base.Update();
    }

    private void ThrowBlock() {
        if (grabbedBlock != null && !hitTimeout.on) {
            Vector2 throwDir;
            if (dir1.magnitude == 0) {
                throwDir = facingDir;
            } else {
                throwDir = dir1.normalized;
            }

            grabbedBlock.BeThrown(throwDir * c.throwBoulderSpeed, c.blockGravity);
            UnGrabBlock();
        }
    }

    public override void BeThrown(Vector2 vel, float gravity) {
        base.BeThrown(vel, gravity);
        hitTimeout.Start();
    }

    private void CalculateScale() {
        dScaleY *= scaleYDecay;
        dScaleY += (targetScaleY - scaleY) * scaleYOmega;
        float variation = dScaleY * Time.deltaTime;
        scaleY += dScaleY * Time.deltaTime;

        body.transform.position += Vector3.up * variation / 2;
        if (Math.Abs(scaleY) < 0.1f) {
            scaleY = 0.1f;
            dScaleY = 0;
        }

        float scaleX = (float)Math.Sqrt(1f / scaleY);
        this.transform.localScale = new Vector2(facingDir.x * Math.Abs(initialSize.x) * scaleX, initialSize.y * scaleY) * normalScale;
    }

    private void ResetWallJumpAndDash() {
        if (standing || onWall) {
            remainingAirJumps = 1;
            remainingDashes = 1;
            //reDashTimeout.Cancel();
        }
    }

    private Vector2 CalculateAirControl(Vector2 vel) {
        if (!dashTimeout.on && !hitTimeout.on && !standing && !doingStep) {
            if (dir1.x != 0 && !holdingWallTimeout.on && !LimitReached(dir1.x, vel.x, c.airLimit)) {
                vel.x += c.airControl * dir1.x;
            }
        }
        return vel;
    }

    private Vector2 CalculateMaxFall(Vector2 vel) {
        if (holdingWall) {
            if (vel.y < c.maxWallFallSpeed) vel.y = c.maxWallFallSpeed;
        } else {
            if (!hitTimeout.on) {
                if (vel.y < c.maxFallSpeed) vel.y = c.maxFallSpeed;
            }
        }
        return vel;
    }

    private void CalculateOnWall() {
        if (!standing && onWall && !doingStep) {
            // Holding wall trigger
            if (dir1.x * normalXSumUnit.x < 0) {
                holdingWallTimeout.Start();
                holdingWall = true;
            }
        }
    }

    private Vector2 CalculateIdle(Vector2 vel) {
        if (!dashTimeout.on && standing && !running && !hitTimeout.on) {
            //if (vel.x < -c.maxRunSpeed) vel.x = -c.maxRunSpeed;
            if (maxStandingFriction < 0.2f) {
                vel.x -= vel.x * (1 - c.runDecrease) * 0.5f * maxStandingFriction;
            } else {
                vel.x -= vel.x * (1 - c.runDecrease) * maxStandingFriction;
            }
        }
        
        return vel;
    }

    private Vector2 CalculateRun(Vector2 vel) {
        //aDebug.Log("Run " + dir1.x);
        running = false;
        if (!hitTimeout.on && !dashTimeout.on && !crouching && standing && dir1.x != 0) {
            //if (dir1.x * vel.x < 0) {
            //    if (maxStandingFriction < 0.2f) {
            //        vel.x -= vel.x * (1 - c.runDecrease) * 0.5f * maxStandingFriction;
            //    } else {
            //        vel.x -= vel.x * (1 - c.runDecrease) * maxStandingFriction;
            //    }
            //}

            float newX = vel.x + dir1.x * c.runIntensity * maxStandingFriction;
            if (newX > c.maxRunSpeed) newX = c.maxRunSpeed;
            if (newX < -c.maxRunSpeed) newX = -c.maxRunSpeed;

            vel.x = newX;

            running = true;
        }

        return vel;
    }

    private Vector2 CalculateJump(Vector2 vel) {
        if (!hitTimeout.on && Controller.GetKeyDown("A", jumpLag)) {
            if (standing) {
                Jump(ref vel);
            } else if (onWall) {
                Jump(ref vel);
                vel.x = c.onWallJumpSpeed.x * Math.Sign(normalXSumUnit.x);
                dashTimeout.Cancel();
            }
        }
        
        return vel;
    }

    private void NormalizeNormals() {
        normalSumUnit.Normalize();
        normalYSumUnit.Normalize();
        normalXSumUnit.Normalize();
    }

    private Vector2 CalculateAirJump(Vector2 vel) {
        if (!hitTimeout.on && remainingAirJumps > 0 && Controller.GetKeyDown("A", jumpLag) && !standing && !onWall) {
            remainingAirJumps--;
            Jump(ref vel);
        }
        return vel;
    }

    private Vector2 CalculateDash(Vector2 vel) {
        if (!hitTimeout.on && (Controller.GetKeyDown("R1", jumpLag) || Controller.GetKeyDown("L1", jumpLag)) && !dashTimeout.on && remainingDashes > 0) {
            dashEffect.Play();
            if (dir1.magnitude <= c.throwThrashold) {
                dashDir = facingDir.normalized;
            } else {
                dashDir = dir1.normalized;
            }

            dashTimeout.Start();
            remainingDashes--;
            //reDashTimeout.Start();
            if (Controller.GetKeyDown("L1", jumpLag)) {
                explodingBlocks = true;
            }
        }

        // Movement during dash
        if (!hitTimeout.on && dashTimeout.on) {
            vel = dashDir * ((c.dash - c.dashEndSpeed) * dashTimeout.remaining / dashTimeout.total + c.dashEndSpeed);
        }
        return vel;
    }

    private void UpdateTimeouts() {
        dashTimeout.Update(Time.deltaTime);
        //reDashTimeout.Update(Time.deltaTime);
        hitTimeout.Update(Time.deltaTime);
        standingTimeout.Update(Time.deltaTime);
        holdingWallTimeout.Update(Time.deltaTime);
    }

    private void FallOnBackground() {
        rotation += rotationSpeed * Time.deltaTime;
        transform.rotation = Quaternion.AngleAxis(rotation, Vector3.forward);
        transform.position += Vector3.down * fallingSpeed * fallingScale * Time.deltaTime;
        transform.localScale = fallingScale * Vector3.one;
    }

    private Vector2 CalculateStepOver(Vector2 vel) {
        float stepSize = 0;
        doingStep = false;
        foreach (var cp1 in hContP) {
            if (cp1.point.y > body.position.y) continue;
            //if (cp1.collider.gameObject.tag == "Player") continue;
            foreach (var cp2 in hContP) {
                if (Vector2.Dot(dir1.x * Vector2.right, cp1.normal) < 0 && Vector2.Dot(cp2.normal, cp1.normal) > 0) {
                    float step = (cp1.point - cp2.point).magnitude;
                    stepSize = Math.Max(stepSize, step);
                }
            }
        }

        if (stepSize > 0 && stepSize < c.stepLimit) {
            if (stepSize < c.fullStepLimit) {
                transform.position += Vector3.up * c.fullStepLimit;
            } else {
                float v = Math.Sign(stepSize) * c.stepPortion;
                if (Math.Abs(v) > Math.Abs(vel.y)) {
                    vel.y = v;
                }
            }

            doingStep = true;
        }

        return vel;
    }

    private void CalculateDirections() {
        dir1Raw = Controller.GetDir1();
        dir1 = dir1Raw;
        if (dir1.magnitude < c.deadzone) {
            dir1 = Vector2.zero;
        }

        if (dir1.x != 0) {
            facingDir = new Vector2(dir1.x, 0);
            facingDir.Normalize();
        }

        if (dir1.magnitude < c.deadzone) {
            grabbedDir = facingDir;
        } else {
            grabbedDir = dir1;
        }
    }

    void ProcessEnterStayContactPoint (ContactPoint2D cP, Collision2D col, bool enter) {
        Debug.DrawLine(cP.point, cP.point + cP.normal * 2);

        Vector2 normalizedCpNormal = cP.normal.normalized;
        GameObject gameObject = GetObject(cP.collider);

        var block = gameObject.GetComponent<Block>();
        var player = block as PlayerControl;

        // Calculate standing
        float normalUpCoef = Vector2.Dot(normalizedCpNormal, Vector2.up);
        if (normalUpCoef > c.standLimit) {
            normalYSumUnit += normalizedCpNormal;

            standing = true;
            standingTimeout.Start();

            if (enter && Math.Abs(col.relativeVelocity.y) > dScaleYThr) {
                //dScaleY = -cP.normal.y * scaleYBounce;
                dScaleY = -Math.Abs(col.relativeVelocity.y) * scaleYBounce;
                if (dScaleY > dScaleYMax) dScaleY = dScaleYMax;
                if (dScaleY < -dScaleYMax) dScaleY = -dScaleYMax;
            }

            if (block.friction > maxStandingFriction)
            {
                maxStandingFriction = block.friction;
            }
        }

        //// Check block to be grabbed
        //Debug.Log(Controller == null);
        //Debug.Log(hitTimeout == null);
        //Debug.Log(block == null);
        if (Controller.GetKeyDown("X", 0.1f) && !hitTimeout.on && !grabbedBlock && block.movable) {
            if ((player != null && normalUpCoef > c.standLimit) || player == null) {
                Vector2 grabDir = dir1;
                if (grabDir.magnitude == 0) {
                    grabDir = new Vector2(Math.Sign(facingDir.x) * 0.5f, -0.7f);
                }

                float a = Math.Max(Math.Abs(grabDir.x), Math.Abs(grabDir.y));
                grabDir /= a;

                Vector2 grabReference = body.position + grabDir;
                Vector2 diff = block.body.position - grabReference;
                float mdiff = diff.magnitude;
                if (bestBlockToGrab == null || mdiff < bestBlockToGrabDistance) {
                    bestBlockToGrab = block;
                    bestBlockToGrabDistance = mdiff;
                }
            }
        }

        // Calculate on wall
        if (Math.Abs(normalUpCoef) < c.wallLimit && block != null && block.wallJumpable) {
            normalXSumUnit += normalizedCpNormal;
            onWall = true;

            if (block.friction > maxWalFriction) {
                maxWalFriction = block.friction;
            }
        }

        normalSumUnit += normalizedCpNormal;

        if (normalUpCoef <= c.horizontalNormalLimit) {
            hContP.Add(cP);
        }
    }

    private static GameObject GetObject(Collider2D collider) {
        var colliderReference = collider.gameObject.GetComponent<ColliderReference>();
        GameObject gameObject;
        if (colliderReference != null) {
            gameObject = colliderReference.TopComponent.gameObject;
        } else {
            gameObject = collider.gameObject;
        }
        return gameObject;
    }

    private void Jump(ref Vector2 vel) {
        standing = false;
        if (grabbedBlock == null) {
            vel.y = c.jumpIntensity;
        } else {
            vel.y = c.jumpIntensity * grabbedBlock.jumpDump;
        }

        dashTimeout.Cancel();
    }

    bool LimitReached(float input, float currentValue, float limit) {
        return (input > 0 && currentValue > limit) || (input < 0 && currentValue < -limit);
    }

    protected override void FixedUpdate() {
        NormalizeNormals();

        CalculateGravity();

        //collisionEnter.Clear();
        //collisionStay.Clear();
        ////if (enter) {
        ////    dScaleY = - cP.normal.y * scaleYBounce;
        ////}

        //float dSpeed = body.velocity.y - prevVel.y;
        //if (Math.Abs(dSpeed) > dScaleYThr) {
        //    dScaleY = - Math.Abs(dSpeed) * scaleYBounce;
        //    if (dScaleY > dScaleYMax) dScaleY = dScaleYMax;
        //    if (dScaleY < -dScaleYMax) dScaleY = -dScaleYMax;
        //}
        
        CleanVariables();
        base.FixedUpdate();
    }

    private void CleanVariables() {
        standing = false;
        onWall = false;
        holdingWall = false;
        normalSumUnit = Vector2.zero;
        normalYSumUnit = Vector2.zero;
        normalXSumUnit = Vector2.zero;
        bestBlockToGrab = null;
        bestBlockToGrabDistance = float.MaxValue;
        hContP.Clear();
        maxStandingFriction = 0;
        maxWalFriction = 0;
    }

    private void CalculateGravity() {
        if (body == null) return;

        if (frozenTimeout.on) {
            body.gravityScale = 0;
            body.velocity = Vector2.zero;
        } else if (dashTimeout.on || doingStep || falling) {
            body.gravityScale = 0;
        }else if (hitTimeout.on) {
            body.gravityScale = c.gravity / 4;
        } else if (Controller != null && Controller.GetKey("A") && body.velocity.y > 0) {
            body.gravityScale = c.gravity / 2;
        } else {
            body.gravityScale = c.gravity;
        }
    }

    protected override void OnCollisionEnter2D(Collision2D col) {
        base.OnCollisionEnter2D(col);

        var block = col.gameObject.GetComponent<Block>();
        if (block != null && block.harmful && block.thrownBy != this) {
            Vector2 diff = this.body.position - block.body.position;
            if (Vector2.Dot(diff.normalized, block.prevVel.normalized) > Mathf.Cos(Mathf.PI / 4)) {
                if (!dashTimeout.on || block.prevVel.magnitude < c.dashSpeedHit) {
                    ////aDebug.Log("I was hit");
                    lastHitBy = block.thrownBy;
                    hitImpact = block.prevVel.normalized;
                    hitTimeout.Start();
                    hitTrail.Play();
                    eyes.Pain();
                    Vector2 vel = hitImpact * c.hitIntensity * this.hitMultiplier;

                    if (normalSumUnit.y > 0.5f) {
                        vel.y += normalSumUnit.y * c.hitIntensity;
                        if (vel.y > c.hitIntensity) vel.y = c.hitIntensity;
                    }

                    body.velocity = vel;

                    UnGrabBlock();

                    // Deviate block trajectory
                    var v = block.body.velocity;
                    v = new Vector2(v.y, -v.x).normalized;
                    block.body.velocity += v * UnityEngine.Random.Range(-1, 1);
                    block.HitPlayer(this);
                    this.hitMultiplier += c.hitIncrease;
                    if (this.hitMultiplier > c.hitMultiplierMax) {
                        this.hitMultiplier = c.hitMultiplierMax;
                    }

                    this.normalScale = Mathf.Sqrt(1f / this.hitMultiplier);
                }
            }
        } else if (block != null && dashTimeout.on && explodingBlocks) {
            GroundBlock gb = block as GroundBlock;

            if (gb != null && gb.breakable) {
                block.body.gravityScale = c.gravity;
                block.body.isKinematic = false;
                gb.Explode(body.velocity, 5, 10);
            }

            remainingAirJumps = 1;
            remainingDashes = 1;
        } else if (hitTimeout.on) {
            // Bounce on collisions
            Vector2 r = col.relativeVelocity.Reflect(col.contacts[0].normal);
            GameObject gameObject = GetObject(col.collider);
            Block b = gameObject.GetComponent<Block>();
            body.velocity = b.prevVel + r * 0.7f;
        }

        foreach (var cp in col.contacts) {
            ProcessEnterStayContactPoint(cp, col, true);
        }
    }

    protected override void OnCollisionStay2D(Collision2D col) {
        base.OnCollisionStay2D(col);
        foreach (var cp in col.contacts) {
            ProcessEnterStayContactPoint(cp, col, false);
        }
    }

    public void Appear(Vector3 pos) {
        transform.position = pos;
        appearAnimation.Animate();
        int index = this.GetComponent<ControllerWrapperBase>().player;
        Debug.Log("Appear " + pos + " " + index);
        bodyObject.GetComponent<SpriteRenderer>().sprite = sprites[index];
    }

    public void Disappear() {
        disappearAnimation.enabled = true;
        body.isKinematic = true;
        this.EnableAllColliders(false);
    }

    public void CleanUp() {
        gameObject.SetActive(true);
        if (body != null) {
            body.velocity = Vector2.zero;
        }

        this.EnableAllColliders(true);

        alive = true;
        dashTimeout.Cancel();
        // reDashTimeout;
        hitTimeout.Cancel();
        standingTimeout.Cancel();
        holdingWallTimeout.Cancel();
        frozenTimeout.Cancel();
        UnGrabBlock();
        
        falling = false;
        lastHitBy = null;
        grabbedBy = null;
        transform.rotation = Quaternion.AngleAxis(0, Vector3.forward);
        SetColor(Color.white);
        SetLayer("Objects");
        dScaleY = 0;
        scaleY = 1;
        this.hitMultiplier = 1;
        this.normalScale = 1;
        eyes.Blink();
    }

    private void SetColor(Color color) {
        foreach (Transform c in transform) {
            var s = c.gameObject.GetComponent<SpriteRenderer>();
            if (s != null) {
                s.color = color;
            }
        }
    }

    private void SetLayer(string layerName) {
        foreach (Transform c in transform) {
            var s = c.gameObject.GetComponent<SpriteRenderer>();
            if (s != null) {
                s.sortingLayerName = layerName;
            }
        }
    }

    void CalculateGrabBlock(Block block) {
        if (!hitTimeout.on && bestBlockToGrab != null && this.grabbedBlock == null && Controller.GetKey("X")) {
            if (block.BeGrabbed(this)) {
                grabbedBlock = block;
            }
        }
    }

    public bool UnGrabBlock() {
        if (grabbedBlock == null) return true;
        if (grabbedBlock.BeUngrabbed(this)) {
            grabbedBlock = null;
            return true;
        }

        return false;
    }

    public void ShowCrown(bool show) {
        crown.enabled = show;
    }

    public void Create(int index, Type ControllerType) {
        var c = this.gameObject.AddComponent(ControllerType) as ControllerWrapperBase;
        c.enabled = true;
        c.player = index;

        foreach (var headItem in headItems) {
            headItem.enabled = false;
        }

        headItems[index].enabled = true;

        // For now set the items as default
        if (index == 1) {
            mouthItems[0].enabled = true;
        } else {
            mouthItems[0].enabled = false;
        }
    }

    public void DiesOutOfStage() {
        alive = false;
        eyes.Close();
        falling = true;
        body.position += Vector2.up;
        body.velocity = Vector2.zero;
        this.EnableAllColliders(false);

        SetColor(new Color(1, 1, 1, 0.5f));
        SetLayer("BackObjects");
    }
}
