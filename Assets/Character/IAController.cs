﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class IAController : ControllerWrapperBase {

    protected Vector2 dir1;

    public IAController() : base() {
        keys = new Dictionary<string, bool>(){
            {"A", false},
            {"B", false},
            {"X", false},
            {"Y", false},
            {"L1", false},
            {"R1", false},
            {"START", false}
        };
    }

    protected bool SetKey(string key, bool pressed) {
        return keys[key] = pressed;
    }

    public override bool GetKeyDown(string key, float timeLimit) {
        return !prevKeys[key] && keys[key];
    }

    public override bool GetKeyUp(string key) {
        return prevKeys[key] && !keys[key];
    }

    public override bool GetKey(string key) {
        return keys[key];
    }

    public override void UpdateKeySet(float dt) {
        Copy(keys, prevKeys);
        PerformIA(dt);
    }

    public override Vector2 GetDir1() {
        return dir1;
    }

    public abstract void PerformIA(float dt);
}
