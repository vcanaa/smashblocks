﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class IAJumper : IAController {
    public int standingCount;

    public override void PerformIA(float dt) {
        PlayerControl player = this.GetComponent<PlayerControl>();
        if (player == null) return;

        dir1 = Vector2.zero;

        if (player.standing) {
            standingCount++;
        } else {
            standingCount = 0;
        }

        if (standingCount % 2 == 0) {
            SetKey("A", true);
        } else {
            SetKey("A", false);
        }

        dir1 = -player.transform.position.normalized;
    }
}
