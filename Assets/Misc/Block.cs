﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Block : Entity {

    // Physics
    public Rigidbody2D body;
    public EdgeCollider2D[] edgeColliders;
    public BoxCollider2D boxCollider;
    public float friction;

    public Vector2 size;

    public PlayerControl grabbedBy;
    public PlayerControl thrownBy;
    public bool harmful;
    public Vector2 prevVel;
    public Vector2 prevPos;
    public PlayerControl lastHitBy;
    public bool wallJumpable;

    public float jumpDump = 1;
    public bool movable;

    public const int Up = 0;
    public const int Right = 1;
    public const int Down = 2;
    public const int Left = 3;

    public Block[] blocksNeighbor;

    public bool sceneBlock;

    public bool awakenedOnce;

    public bool useBoxCollider;

    HashSet<EnableCollision> blocksToEnableColision = new HashSet<EnableCollision>();
    List<EnableCollision> enabledBlocks = new List<EnableCollision>();

    public Vector2 grabbedDir;

    //HashSet<Collision2D> collisions = new HashSet<Collision2D>();

    protected virtual void Awake() {
        if (!awakenedOnce) {
            boxCollider = this.gameObject.AddComponent<BoxCollider2D>();
            boxCollider.size = size;
            boxCollider.sharedMaterial = new PhysicsMaterial2D();

            edgeColliders = new EdgeCollider2D[4];
            blocksNeighbor = new Block[4];
            float hX = size.x / 2;
            float hY = -size.y / 2;
            edgeColliders[0] = CreateEdge(new Vector2[] {
                new Vector2 { x = -hX, y = -hY},
                new Vector2 { x = hX, y = -hY}
            });

            edgeColliders[1] = CreateEdge(new Vector2[] {
                new Vector2 { x = hX, y = -hY},
                new Vector2 { x = hX, y = hY}
            });

            edgeColliders[2] = CreateEdge(new Vector2[] {
                new Vector2 { x = hX, y = hY},
                new Vector2 { x = -hX, y = hY}
            });

            edgeColliders[3] = CreateEdge(new Vector2[] {
                new Vector2 { x = -hX, y = hY},
                new Vector2 { x = -hX, y = -hY}
            });

            body = this.gameObject.AddComponent<Rigidbody2D>();
            body.fixedAngle = true;
            body.collisionDetectionMode = CollisionDetectionMode2D.Continuous;

            EntityHub.Tag(this, Tags.SceneItem);

            if (sceneBlock) {
                EntityHub.Tag(this, Tags.SceneBlock);
            }

            UseBoxCollider(false);

            awakenedOnce = true;
        }
    }

    //private EdgeCollider2D CreateEdge(float angle) {
    //    var points = new Vector2[] {
    //        new Vector2(0, -0.5f),
    //        new Vector2(0, 0.5f)
    //    };

    //    var obj = new GameObject();
    //    var collider = obj.AddComponent<EdgeCollider2D>();
    //    //collider.usedByEffector = true;
    //    //var effect = obj.AddComponent<PlatformEffector2D>();
    //    //effect.useOneWay = true;
    //    var colliderReference = obj.AddComponent<ColliderReference>();
    //    colliderReference.TopComponent = this;
    //    obj.transform.parent = this.transform;
    //    obj.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    //    collider.sharedMaterial = physMaterial;
    //    collider.points = points;
    //    float radAngle = angle / 180 * (float)Math.PI;
    //    obj.transform.localPosition = new Vector3(Mathf.Cos(radAngle) * size.x / 2, Mathf.Sin(radAngle) * size.y / 2, 0);
    //    return collider;
    //}

    class EnableCollision {
        public float time;
        public Block block;
    }

    private EdgeCollider2D CreateEdge(Vector2[] points) {
        var collider = this.gameObject.AddComponent<EdgeCollider2D>();
        collider.sharedMaterial = boxCollider.sharedMaterial;
        collider.points = points;
        return collider;
    }

    public override void Update() {
        if (body.velocity.magnitude < 0.01 && grabbedBy != null) {
            harmful = false;
        }

        if (grabbedBy != null && movable) {
            Bounds bounds = this.bounds;
            var size = bounds.size;
            //var top = grabbedBy.bounds.max.y;
            //transform.position = new Vector3(grabbedBy.transform.position.x, size.y / 2 * 1.1f + top, 0);

            //bounds.center = grabbedBy.transform.position + (grabbedBy.grabbedDir.normalized * (size.x + grabbedBy.size.x) / 2).ToVector3();

            int stepLimit = 20;
            //float step = (size.x + grabbedBy.size.x) / 2 / stepLimit;
            float step = grabbedBy.size.x / 2 / stepLimit;

            Collider2D[] colliders;
            int tries = 0;
            //Vector3 dir = grabbedBy.grabbedDir.normalized;
            Vector3 dir = new Vector2(Mathf.Sign(grabbedBy.grabbedDir.x), 1);
            bounds.center = grabbedBy.transform.position;

            while (tries < stepLimit) {
                tries++;
                bounds.center = bounds.center + dir * step;
                colliders = Physics2D.OverlapAreaAll(bounds.min, bounds.max);

                Vector2 s = Vector2.zero;
                foreach (Collider2D c in colliders) {
                    BoxCollider2D cc = c as BoxCollider2D;
                    if (cc == null) continue;
                    if (cc.gameObject == this.gameObject) continue;
                    if (cc.gameObject == grabbedBy.gameObject) continue;

                    bounds.center = MoveOut(cc.bounds, bounds, 0.00001f);
                }

                //dir = bounds.center - grabbedBy.bounds.center;
                //dir.Normalize();
            }

            transform.position = bounds.center;
        }

        enabledBlocks.Clear();
        foreach (EnableCollision block in blocksToEnableColision) {
            block.time += Time.deltaTime;
            if (TryEnableColision(block)) {
                enabledBlocks.Add(block);
            }
        }

        foreach (EnableCollision block in enabledBlocks) {
            blocksToEnableColision.Remove(block);
        }

        //if (grabbedBy != null) {
        //    boxCollider.size = Vector2.one * 0.5f;
        //} else {
        //    boxCollider.size = Vector2.one;
        //}
    }

    // Gets where a raycast touch the edge off the box from the center.
    public void GetLimit(Bounds bounds, ref Vector2 dir) {
        if (dir.x == 0 && dir.y == 0) dir = bounds.center + (Vector3.right * (bounds.size.x / 2));

        if (dir.x > dir.y) {
            dir *= bounds.size.x / dir.x / 2;
        } else {
            dir *= bounds.size.y / dir.y / 2;
        }
    }

    public Vector2 MoveOut(Bounds bounds, Bounds bounds2, float offset) {
        Vector3 dir = bounds2.center - bounds.center;

        if (dir.x == 0 && dir.y == 0) return bounds2.center;

        //if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y)) {
        //    return bounds.center + dir.normalized * (bounds.size.x + bounds2.size.x + offset) / 2;
        //} else {
        //    return bounds.center + dir.normalized * (bounds.size.y + bounds2.size.y + offset) / 2;
        //}

        if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y)) {
            if (Mathf.Abs(dir.x) >= (bounds.size.x + bounds2.size.x) / 2 + offset) return bounds2.center;
            float target = (bounds.size.x + bounds2.size.x) / 2 + offset;
            return new Vector2(target * Mathf.Sign(dir.x) + bounds.center.x, bounds2.center.y);
        } else {
            if (Mathf.Abs(dir.y) >= (bounds.size.y + bounds2.size.y) / 2 + offset) return bounds2.center;
            float target = (bounds.size.y + bounds2.size.y) / 2 + offset;
            return new Vector2(bounds2.center.x, target * Mathf.Sign(dir.y) + bounds.center.y);
        }
    }

    protected virtual void Start() {
    }

    protected virtual void FixedUpdate() {
        if (body == null) return;
        prevVel = body.velocity;
        prevPos = body.position;
        //collisions.Clear();
    }

    protected virtual void OnCollisionEnter2D(Collision2D col) {
        //collisions.Add(col);
    }

    protected virtual void OnCollisionStay2D(Collision2D col) {
        //collisions.Add(col);
    }

    //protected virtual void OnCollisionStay2D(Collision2D col) {
    //    // Calculate Friction
    //    if (this.body.isKinematic) return;

    //    Vector2 tangent = col.contacts[0].normal.Normal().normalized;
    //    float x = Mathf.Sign(Vector2.Dot(tangent, col.relativeVelocity.normalized));
    //    this.body.velocity -= tangent * x * friction;
    //}

    public Bounds bounds {
        get {
            return new Bounds(this.transform.position, new Vector3(size.x * this.transform.localScale.x, size.y * this.transform.localScale.y, 0));
            //return boxCollider.bounds;
        }
    }

    // Attempt to connect 
    public bool Connect(Block other) {
        float connectSize = (this.size.x + other.size.x) / 2;
        Vector2 d = other.transform.position - this.transform.position;
        if (Math.Abs(d.x) <= connectSize / 10 && d.y >= -connectSize && d.y < 0) {
            Connect(other, Down);
            other.Connect(this, Oposite(Down));
            //Debug.Log("Connect Down");
            return true;
        } else if (Math.Abs(d.x) <= connectSize / 10 && d.y <= connectSize && d.y > 0) {
            Connect(other, Up);
            other.Connect(this, Oposite(Up));
            //Debug.Log("Connect Up");
            return true;
        } else if (Math.Abs(d.y) <= connectSize / 10 && d.x >= -connectSize && d.x < 0) {
            Connect(other, Left);
            other.Connect(this, Oposite(Left));
            //Debug.Log("Connect Left");
            return true;
        } else if (Math.Abs(d.y) <= connectSize / 10 && d.x <= connectSize && d.x > 0) {
            Connect(other, Right);
            other.Connect(this, Oposite(Right));
            //Debug.Log("Connect Right");
            return true;
        } else {
            //Debug.Log("Cant connect: " + d + " " + connectSize);
        }

        return false;
    }

    private static int Oposite(int direction) {
        return (direction + 2) % 4;
    }

    private void Connect(Block other, int direction) {
        blocksNeighbor[direction] = other;
        edgeColliders[direction].isTrigger = true;
    }

    public virtual void BeThrown(Vector2 vel, float gravity) {
        body.velocity = vel;
        lastHitBy = grabbedBy;
        //IgnoreCollision(grabbedBy, true);
        //StartCoroutine(EnableColision(grabbedBy));
        blocksToEnableColision.Add(new EnableCollision { block = grabbedBy });
        thrownBy = grabbedBy;
        grabbedBy = null;
        harmful = true;
        //aDebug.Log("Thrown " + gravity);
        body.gravityScale = gravity;
        body.isKinematic = false;
        boxCollider.isTrigger = false;
    }

    public void IgnoreCollision(Block other, bool ignore) {
        //var colliders1 = this.GetComponents<Collider2D>();
        //var colliders2 = this.GetComponents<Collider2D>();

        //foreach (var c1 in colliders1) {
        //    foreach (var c2 in colliders2) {
        //        Debug.Log("Ignore collision " + ignore);
        //        Physics2D.IgnoreCollision(c1, c2, ignore);
        //    }
        //}

        Physics2D.IgnoreCollision(boxCollider, other.boxCollider, ignore);
    }

    public virtual bool BeGrabbed(PlayerControl player) {
        if (!movable) return false;

        if (grabbedBy != null) {
            grabbedBy.UnGrabBlock();
        }

        grabbedBy = player;
        lastHitBy = player;
        UseBoxCollider(true);
        EnableAllColliders(true);
        IgnoreCollision(player, true);
        boxCollider.isTrigger = true;
        Disconnect();

        return true;
    }

    public void Disconnect() {
        for (int i = 0; i < 4; i++) {
            Disconnect(i);
        }
    }

    public void Disconnect(int direction, bool callDisconnect = true) {
        Block b = blocksNeighbor[direction];
        if (b != null) {
            edgeColliders[direction].isTrigger = useBoxCollider;
            if (callDisconnect) {
                b.Disconnect((direction + 2) % 4, false);
            }
        }
    }

    public void UseBoxCollider(bool useBoxCollider) {
        this.useBoxCollider = useBoxCollider;
        boxCollider.isTrigger = !useBoxCollider;
        foreach (var c in edgeColliders) {
            c.isTrigger = useBoxCollider;
        }
    }

    public void EnableAllColliders(bool enable) {
        foreach (var collider in edgeColliders) {
            collider.isTrigger = !(enable && !useBoxCollider);
        }

        boxCollider.isTrigger = !(enable && useBoxCollider);
    }

    public virtual bool BeUngrabbed(PlayerControl player) {
        //IgnoreCollision(player, false);
        if (grabbedBy == player) {
            grabbedBy = null;
        }

        boxCollider.isTrigger = false;

        return true;
    }

    bool TryEnableColision(EnableCollision other) {
        Bounds b1 = other.block.boxCollider.bounds;
        Bounds b2 = this.boxCollider.bounds;
        
        if (other.time > 0.4f && !Intersects(b1, b2)) {
            IgnoreCollision(other.block, false);
            return true;
        }

        return false;
    }

    bool Intersects(Bounds b1, Bounds b2) {
        if (b1.min.x > b2.max.x) return false;
        if (b1.max.x < b2.min.x) return false;
        if (b1.min.y > b2.max.y) return false;
        if (b1.max.y < b2.min.y) return false;

        return true;
    }

    public virtual void HitPlayer(PlayerControl player) { }
}
