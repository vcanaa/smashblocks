﻿using UnityEngine;
using System.Collections;

public class ShapeAnimation : MonoBehaviour {

    public FuncSin sinX;
    public FuncSin sinY;
    public FuncExp exp;

    public Timeout timeout;

    Vector3 baseScale;

	void Start () {
        baseScale = transform.localScale;
        timeout.callback = End;
        timeout.Start();
	}
	
	void Update () {
        timeout.Update(Time.deltaTime);

        float scaleX = baseScale.x * (1 + sinX.V(timeout.timePortion) * exp.V(timeout.timePortion));
        float scaleY = baseScale.y * (1 + sinY.V(timeout.timePortion) * exp.V(timeout.timePortion));

        transform.localScale = new Vector3(scaleX, scaleY, baseScale.z);
	}

    void End() {
        transform.localScale = baseScale;
    }
}
