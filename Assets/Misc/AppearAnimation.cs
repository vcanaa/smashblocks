﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class AppearAnimation : MonoBehaviour {
    public new string name;
    public FuncSin sinX;
    public FuncSin sinY;
    public FuncExp exp;
    public FuncExp growExp;

    public Timeout timeout;

    Vector3 baseScale;
    public Vector2 finalScale;

    public bool deleteOnFinish;

    public bool start;

    public void Awake() {
        baseScale = transform.localScale;
        timeout.callback = End;
    }

    public void Start() {
        if (start) {
            timeout.Start();
        }
    }

    void Update() {
        timeout.Update(Time.deltaTime);

        if (timeout.on) {
            DoIt();
        }
    }

    private void DoIt() {
        float x = timeout.timePortion;
        float scaleX = baseScale.x * (1 + sinX.V(x) * exp.V(x)) * growExp.V(x);
        float scaleY = baseScale.y * (1 + sinY.V(x) * exp.V(x)) * growExp.V(x);

        transform.localScale = new Vector3(scaleX, scaleY, baseScale.z);
    }

    void End() {
        transform.localScale = finalScale;
        this.enabled = false;
        if (deleteOnFinish) {
            Destroy(this.gameObject);
        }
    }

    public void Animate() {
        enabled = true;
        timeout.Start();
        DoIt();
    }
}
