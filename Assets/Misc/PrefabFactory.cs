﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PrefabFactory : MonoBehaviour {
    public static PrefabFactory get;

    public List<KeyValueGameObject> x;

    void Awake() {
        if (get != null) {
            throw new Exception("PrefabFactory must be unique");
        }

        get = this;
    }
}