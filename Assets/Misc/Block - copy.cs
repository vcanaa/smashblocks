﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using UnityEngine;

//public class Block : Entity {

//    // Physics
//    public Rigidbody2D body;
//    public EdgeCollider2D[] edgeColliders;
//    public BoxCollider2D boxCollider;
//    public PhysicsMaterial2D physMaterial;

//    public Vector2 size;

//    public PlayerControl grabbedBy;
//    public PlayerControl thrownBy;
//    public bool harmful;
//    public Vector2 prevVel;
//    public Vector2 prevPos;
//    public PlayerControl lastHitBy;
//    public bool wallJumpable;

//    public float jumpDump = 1;
//    public bool movable;

//    public float friction;

//    public const int Up = 0;
//    public const int Right = 1;
//    public const int Down = 2;
//    public const int Left = 3;

//    public Block[] blocksNeighbor;

//    public bool sceneBlock;

//    public bool awakenedOnce;

//    public bool useBoxCollider;

//    protected virtual void Awake() {
//        if (!awakenedOnce) {
//            edgeColliders = new EdgeCollider2D[4];
//            blocksNeighbor = new Block[4];
//            float hX = size.x / 2;
//            float hY = -size.y / 2;
//            edgeColliders[0] = CreateEdge(new Vector2[] {
//                new Vector2 { x = -hX, y = -hY},
//                new Vector2 { x = hX, y = -hY}
//            });

//            edgeColliders[1] = CreateEdge(new Vector2[] {
//                new Vector2 { x = hX, y = -hY},
//                new Vector2 { x = hX, y = hY}
//            });

//            edgeColliders[2] = CreateEdge(new Vector2[] {
//                new Vector2 { x = hX, y = hY},
//                new Vector2 { x = -hX, y = hY}
//            });

//            edgeColliders[3] = CreateEdge(new Vector2[] {
//                new Vector2 { x = -hX, y = hY},
//                new Vector2 { x = -hX, y = -hY}
//            });

//            body = this.gameObject.AddComponent<Rigidbody2D>();
//            body.fixedAngle = true;
//            body.collisionDetectionMode = CollisionDetectionMode2D.Continuous;

//            boxCollider = this.gameObject.AddComponent<BoxCollider2D>();
//            boxCollider.size = size;
//            boxCollider.sharedMaterial = physMaterial;

//            EntityHub.Tag(this, Tags.SceneItem);

//            if (sceneBlock) {
//                EntityHub.Tag(this, Tags.SceneBlock);
//            }

//            UseBoxCollider(false);

//            awakenedOnce = true;
//        }
//    }

//    private EdgeCollider2D CreateEdge(Vector2[] points) {
//        var collider = this.gameObject.AddComponent<EdgeCollider2D>();
//        collider.sharedMaterial = physMaterial;
//        collider.points = points;
//        return collider;
//    }

//    public override void Update() {
//        if (body.velocity.magnitude < 0.01 && grabbedBy != null) {
//            harmful = false;
//        }

//        if (grabbedBy != null && movable) {
//            var size = bounds.size;
//            var top = grabbedBy.bounds.max.y;
//            transform.position = new Vector3(grabbedBy.transform.position.x, size.y / 2 * 1.1f + top, 0);
//            //Vector2 targetPos = new Vector3(grabbedBy.transform.position.x, size.y / 2 * 1.1f + top, 0);
//            //Vector2 v = (targetPos - body.position) / Time.fixedDeltaTime;
//            //body.velocity = grabbedBy.body.velocity + v;
//            //body.gravityScale = 0;
//            //body.velocity = v;
//        }

//        //if (grabbedBy != null) {
//        //    boxCollider.size = Vector2.one * 0.5f;
//        //} else {
//        //    boxCollider.size = Vector2.one;
//        //}
//    }

//    protected virtual void Start() {
//    }

//    protected virtual void FixedUpdate() {
//        if (body == null) return;
//        prevVel = body.velocity;
//        prevPos = body.position;
//    }

//    public Bounds bounds {
//        get {
//            return new Bounds(this.transform.position, new Vector3(size.x * this.transform.localScale.x, size.y * this.transform.localScale.y, 0));
//            //return boxCollider.bounds;
//        }
//    }

//    // Attempt to connect 
//    public bool Connect(Block other) {
//        float connectSize = (this.size.x + other.size.x) / 2;
//        Vector2 d = other.transform.position - this.transform.position;
//        if (Math.Abs(d.x) < connectSize / 10 && d.y > -connectSize && d.y < 0) {
//            blocksNeighbor[Down] = other;
//            edgeColliders[Down].isTrigger = true;
//            return true;
//        } else if (Math.Abs(d.x) < connectSize / 10 && d.y < connectSize && d.y > 0) {
//            blocksNeighbor[Up] = other;
//            edgeColliders[Up].isTrigger = true;
//            return true;
//        } else if (Math.Abs(d.y) < connectSize / 10 && d.x > -connectSize && d.x < 0) {
//            blocksNeighbor[Left] = other;
//            edgeColliders[Left].isTrigger = true;
//            return true;
//        } else if (Math.Abs(d.y) < connectSize / 10 && d.x < connectSize && d.x > 0) {
//            blocksNeighbor[Right] = other;
//            edgeColliders[Right].isTrigger = true;
//            return true;
//        }

//        return false;
//    }

//    public virtual void BeThrown(Vector2 vel, float gravity) {
//        body.velocity = vel;
//        lastHitBy = grabbedBy;
//        //IgnoreCollision(grabbedBy, true);
//        StartCoroutine(EnableColision(grabbedBy));
//        thrownBy = grabbedBy;
//        grabbedBy = null;
//        harmful = true;
//        //aDebug.Log("Thrown " + gravity);
//        body.gravityScale = gravity;
//        body.isKinematic = false;
//    }

//    public void IgnoreCollision(Block other, bool ignore) {
//        //var colliders1 = this.GetComponents<Collider2D>();
//        //var colliders2 = this.GetComponents<Collider2D>();

//        //foreach (var c1 in colliders1) {
//        //    foreach (var c2 in colliders2) {
//        //        Debug.Log("Ignore collision " + ignore);
//        //        Physics2D.IgnoreCollision(c1, c2, ignore);
//        //    }
//        //}

//        Physics2D.IgnoreCollision(boxCollider, other.boxCollider, ignore);
//    }

//    public virtual bool BeGrabbed(PlayerControl player) {
//        if (!movable) return false;

//        if (grabbedBy != null) {
//            grabbedBy.UnGrabBlock();
//        }

//        grabbedBy = player;
//        lastHitBy = player;
//        UseBoxCollider(true);
//        EnableAllColliders(true);
//        IgnoreCollision(player, true);

//        Disconnect();

//        return true;
//    }

//    public void Disconnect() {
//        for (int i = 0; i < 4; i++) {
//            Disconnect(i);
//        }
//    }

//    public void Disconnect(int direction, bool callDisconnect = true) {
//        Block b = blocksNeighbor[direction];
//        if (b != null) {
//            edgeColliders[direction].isTrigger = useBoxCollider;
//            if (callDisconnect) {
//                b.Disconnect((direction + 2) % 4, false);
//            }
//        }
//    }

//    public void UseBoxCollider(bool useBoxCollider) {
//        this.useBoxCollider = useBoxCollider;
//        boxCollider.isTrigger = !useBoxCollider;
//        foreach (var c in edgeColliders) {
//            c.isTrigger = useBoxCollider;
//        }
//    }

//    public void EnableAllColliders(bool enable) {
//        foreach (var collider in edgeColliders) {
//            collider.isTrigger = !(enable && !useBoxCollider);
//        }

//        boxCollider.isTrigger = !(enable && useBoxCollider);
//    }

//    public virtual bool BeUngrabbed(PlayerControl player) {
//        //IgnoreCollision(player, false);
//        if (grabbedBy == player) {
//            grabbedBy = null;
//        }

//        return true;
//    }

//    IEnumerator EnableColision(PlayerControl player) {
//        yield return new WaitForSeconds(0.2f);
//        if (player.gameObject.activeSelf) {
//            IgnoreCollision(player, false);
//        }
//    }

//    public virtual void HitPlayer(PlayerControl player) { }
//}
