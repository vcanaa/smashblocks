﻿using UnityEngine;
using System.Collections;
using System;

public class PointSymbol : MonoBehaviour {

    public Vector3 startingPos;
    public float timeToLive;
    Timeout liveTimeout;

    public Transform attachedTo;
    public FuncExp func;

	void Start () {
        if (this.attachedTo == null) {
            this.transform.localPosition = startingPos;
            this.attachedTo = this.transform;
        }

	    liveTimeout = new Timeout{
            total = timeToLive,
            callback = Die
        };

        liveTimeout.Start();
	}
	
	void Update () {
        liveTimeout.Update(Time.deltaTime);

        DoIt();
	}

    private void DoIt() {
        this.transform.position = attachedTo.position + startingPos + Vector3.up * func.V(liveTimeout.time / liveTimeout.total);
    }

    void Die() {
        Destroy(this.gameObject);
    }

    public void Attach(Transform attachedTo) {
        this.attachedTo = attachedTo;
        this.transform.position = attachedTo.position + startingPos;
    }
}
