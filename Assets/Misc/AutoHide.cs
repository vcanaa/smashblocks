﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class AutoHide : MonoBehaviour {
    void Awake() {
        var sprite = this.GetComponent<SpriteRenderer>();
        sprite.enabled = false;
    }
}