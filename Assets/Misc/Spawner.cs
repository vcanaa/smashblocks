﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Spawner : MonoBehaviour {
    public Timeout timer;
    public GameObject spawn;

    void Start() {
        timer.Start();
        timer.callback = () => {
            timer.Start();
            GameObject.Instantiate(spawn, this.transform.position, Quaternion.identity);
        };
    }

    void Update() {
        timer.Update(Time.deltaTime);
    }
}
