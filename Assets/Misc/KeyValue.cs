﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class KeyValueGameObject {
    public string key;
    public GameObject value;
}
