﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Block), true)]
public class BlockEditor : Editor {
    bool prevIsMouseDoown;

    void OnEnable() {
        SceneView.onSceneGUIDelegate += OnSceneGui;
    }

    void OnDisable() {
        SceneView.onSceneGUIDelegate -= OnSceneGui;
    }

    protected virtual void OnSceneGui(SceneView view) {
        Snap();
    }

    protected void Snap() {
        var t = (Block)target;
        if (Event.current.type == EventType.mouseUp || Event.current.type == EventType.mouseDown) {
            var pos = t.transform.position;
            pos.x = MathUtil.RoundCloser(pos.x, t.bounds.size.x);
            pos.y = MathUtil.RoundCloser(pos.y, t.bounds.size.y);
            t.transform.position = pos;
        }
    }
}
