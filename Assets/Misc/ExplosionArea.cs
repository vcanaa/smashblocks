﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExplosionArea : MonoBehaviour {

    public HashSet<Collider2D> colliders;

    void Awake() {
        colliders = new HashSet<Collider2D>();
    }

    void FixedUpdate() {
        colliders.Clear();
    }

    void OnTriggerStay2D(Collider2D col) {
        colliders.Add(col);
    }

    void OnTriggerEnter2D(Collider2D col) {
        colliders.Add(col);
    }
}
