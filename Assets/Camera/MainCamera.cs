﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {

    public Target target;

    public Vector2 pos;
    public Vector2 vel;

    public float zoom;

	void Start () {
	    
	}
	
	void Update () {
        pos += vel * Time.deltaTime;

        transform.position = pos.ToVector3(transform.position.z);
	}

    public void Focus(Target target) {
        this.target = target;
    }
}
