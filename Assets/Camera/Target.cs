﻿using UnityEngine;

public class Target {
    public Vector2 Pos;
    public Vector3 Vel;
    public virtual void Update() { }
}
