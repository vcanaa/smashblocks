﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class Tags {

    // Item that is placed in the editor and builds the scene. These items are restarted every round.
    public const string SceneItem = "sceneitem";

    // Blocks that are connected
    public const string SceneBlock = "sceneblock";

    // Items that can't be destroyed when the scene restats.
    public const string SpawnerItem= "spawneritem";
}
