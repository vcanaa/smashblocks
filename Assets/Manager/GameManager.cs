﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameManager : MonoBehaviour {
    public int numberOfPlayers;
    public Vector3 playerPosition;
    public float playerDistance;

    public Bounds deathBounds;

    List<PlayerControl> players;
    public static GameManager get;

    // Prefabs
    public PointSymbol pointSymbolPrefab;
    public PlayerControl playerPrefab;

    public Vector2 trophyPosition;
    PlayerControl winner;

    public ScenarioManager initManager;

    public SpawningPoints spawningPoints;

    public Timeout postGameTimeout;
    bool restart;

    int stageIndex;
    public string[] stages;

    void Awake() {
        if (get != null)
        {
            Destroy(this.gameObject);
            return;
        }

        get = this;
        stages = new string[] { "Stage01", "Stage02" };
    }

	void Start () {
        Debug.Log("GameManager Start");
        numberOfPlayers = Math.Min(numberOfPlayers, spawningPoints.points.Count);
        players = new List<PlayerControl>();
        for (int i = 0; i < numberOfPlayers; i++) {
            var player = GameObject.Instantiate<PlayerControl>(playerPrefab);

            Type controllerType;
            if (i == 4) {
                controllerType = typeof(IAJumper);
            } else {
                controllerType = typeof(ControllerWrapper);
            }

            player.Create(i, controllerType);
            player.gameObject.SetActive(false);
            players.Add(player);
        }

        postGameTimeout.callback = () => { restart = true; };

        initManager = new ScenarioManager();
        initManager.Start();

        StartCoroutine(UpdateInternal());
	}

    IEnumerator InitiateRestart() {
        yield return new WaitForSeconds(1);
        RespawnPlayers();
        RestartScenario();

        yield return new WaitForSeconds(1);
        RestartMatch();
    }

    void RestartScenario() {
        initManager.RestartScenario();
    }

    void RespawnPlayers() {
        for (int i = 0; i < players.Count; i++) {
            //aDebug.Log("Instantiate Players: " + i);
            InitiatePlayer(players[i], i);
        }
    }

    void RestartMatch() {
        winner = null;
        restart = false;
        checkDead = true;
    }

    private void LoadNextScene() {
        stageIndex++;
        if (stageIndex >= stages.Length) {
            stageIndex = 0;
        }

        Debug.Log("Loading stage " + stages[stageIndex]);
        Application.LoadLevel(stages[stageIndex]);
    }

    void InitiatePlayer(PlayerControl player, int i) {
        Debug.Log("InitiatePlayer " + i);
        player.CleanUp();
        player.Appear(spawningPoints.GetPos(i));
        player.frozenTimeout.Start();
    }

    PlayerControl playerAlive;
    bool checkDead;

	IEnumerator UpdateInternal () {
        //aDebug.Log("Start");
        yield return 0;

        yield return StartCoroutine(InitiateRestart());

        while(true) {
            if (restart) {
                //yield return new WaitForSeconds(2);
                //LoadNextScene();
                yield return StartCoroutine(InitiateRestart());
            }

            int numberPlayersDead = 0;
            playerAlive = null;

            // Update players
            foreach (var player in players) {
                if (Input.GetKeyDown(KeyCode.A)) {
                    //aDebug.Log("Create PointSymbol");
                    var ps = GameObject.Instantiate<PointSymbol>(pointSymbolPrefab);
                    ps.transform.parent = players[0].transform;
                }

                if (!player.alive) {
                    numberPlayersDead++;
                    continue;
                }

                if (!deathBounds.Contains(player.transform.position) && player.hitTimeout.on || player.transform.position.y < deathBounds.min.y) {
                    string msg = "Player died: " + player.GetComponent<ControllerWrapperBase>().player;
                    player.DiesOutOfStage();
                    numberPlayersDead++;
                    
                    if (player.lastHitBy != null) {
                        msg += " by " + player.lastHitBy.GetComponent<ControllerWrapperBase>().player;
                        var ps = GameObject.Instantiate<PointSymbol>(pointSymbolPrefab);
                        ps.Attach(player.lastHitBy.transform);
                    }

                    player.ShowCrown(false);

                    //aDebug.Log(msg);
                } else {
                    playerAlive = player;
                }
            }

            //aDebug.Log("Players alive " + numberPlayersDead);

            if (checkDead) {
                // Check for lone survivor
                if (numberPlayersDead == players.Count) {
                    //aDebug.Log("End of game: Draw");
                    postGameTimeout.Start();
                    checkDead = false;
                }

                if (numberPlayersDead == players.Count - 1) {
                    playerAlive.ShowCrown(true);
                    //aDebug.Log("End of game: " + playerAlive.GetComponent<ControllerWrapperBase>().player);
                    postGameTimeout.Start();
                    checkDead = false;
                }
            }
            
            
            postGameTimeout.Update(Time.deltaTime);

            yield return 0;
        }
	}
}
