﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScenarioManager {

    ICollection<Entity> spawnerItems;

    public static ScenarioManager get;

    public void Start() {
        if (get != null) {
            throw new Exception();
        }

        get = this;

        spawnerItems = EntityHub.Get(Tags.SceneItem).ToList();
        foreach (var entity in spawnerItems) {
            EntityHub.Tag(entity, Tags.SpawnerItem);
            EntityHub.Untag(entity, Tags.SceneBlock);
            EntityHub.Untag(entity, Tags.SceneItem);
        }

        spawnerItems = EntityHub.Get(Tags.SpawnerItem);

        RestartScenario();
    }

    public void RestartScenario() {
        Debug.Log("RegenerateBlocks");
        var sceneItems = EntityHub.Get(Tags.SceneItem);

        foreach (var item in sceneItems) {
            EntityHub.EntitiesToDestroy.Add(item);
        }

        EntityHub.DestroyEntities();

        var _spawner = new List<Entity>(spawnerItems);

        foreach (var b in _spawner) {
            CopyEntity(b);
        }

        List<Block> blocks = EntityHub.Get(Tags.SceneBlock).Select(x => x as Block).ToList();

        Debug.Log("Connect blocks: " + blocks.Count);

        for (int i = 0; i < blocks.Count; i++) {
            Block b1 = blocks[i];

            for (int j = i + 1; j < blocks.Count; j++) {
                Block b2 = blocks[j];
                //Debug.Log("Try connect");
                b1.Connect(b2);
            }
        }
    }

    private void CopyEntity(Entity spawnerObject) {
        Entity newObject = spawnerObject.Copy();
        EntityHub.Untag(newObject, Tags.SpawnerItem);
        Block block = newObject.gameObject.GetComponent<Block>();
        if (block != null && block.sceneBlock) {
            EntityHub.Tag(newObject, Tags.SceneBlock);
        }

        EntityHub.Tag(newObject, Tags.SceneItem);
        newObject.gameObject.SetActive(true);
        spawnerObject.gameObject.SetActive(false);
    }
}
