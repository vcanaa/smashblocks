﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

// All entities are here. Enables finding entities by tags.
public static class EntityHub {
    public static Dictionary<string, HashSet<Entity>> entities = new Dictionary<string, HashSet<Entity>>();
    public static HashSet<Entity> allEntities = new HashSet<Entity>();

    public static HashSet<Entity> EmptyHashSet = new HashSet<Entity>();

    public static List<Entity> EntitiesToDestroy = new List<Entity>();

    public static void DestroyEntities() {
        Debug.Log("Destroying entitties " + EntitiesToDestroy.Count);
        foreach (var entity in EntitiesToDestroy) {
            entity.DestroyGameObject();
        }

        EntitiesToDestroy.Clear();
    }

    public static void Tag(Entity entity, string tag) {
        HashSet<Entity> ents;
        if (!entities.TryGetValue(tag, out ents)) {
            ents = new HashSet<Entity>();
            entities.Add(tag, ents);
        }

        ents.Add(entity);
        entity.tags.Add(tag);
    }

    public static void Untag(Entity entity, string tag) {
        HashSet<Entity> ents;
        if (!entities.TryGetValue(tag, out ents)) {
            return;
        }

        ents.Remove(entity);
        entity.tags.Remove(tag);
    }

    public static void Add(Entity entity) {
        allEntities.Add(entity);
    }

    public static void Remove(Entity entity) {
        allEntities.Remove(entity);
        foreach (string tag in entity.tags) {
            entities[tag].Remove(entity);
        }
    }

    public static HashSet<Entity> Get(string tag) {
        HashSet<Entity> ents;

        if (!entities.TryGetValue(tag, out ents)) {
            ents = new HashSet<Entity>();
            entities.Add(tag, ents);
        }

        return ents;
    }
}
