﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Entity : MonoBehaviour {
    public HashSet<string> tags;

    public bool DestroyOnOutOfBounds;

    public bool Destroyed;

    public Entity() {
        this.tags = new HashSet<string>();
    }

    public bool HasTag(string tag) {
        return tags.Contains(tag);
    }

    public virtual void Update()
    {
    }

    public void DestroyGameObject() {
        Destroyed = false;
        EntityHub.Remove(this);
        Destroy(this.gameObject);
    }

    public Entity Copy() {
        var newObj = GameObject.Instantiate(this);
        foreach (string tag in this.tags) {
            EntityHub.Tag(newObj, tag);
        }

        return newObj;
    }
}
