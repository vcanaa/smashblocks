﻿using UnityEngine;
using System.Collections;
using System;


public class MovingGround : MonoBehaviour {

    public GroundGenerator groundGenerator;

    public Vector2 size;
    public Vector2 startPos;
    public Vector2 endPos;
    public Vector2 layer;
    public Vector2 prevPos;

    Rigidbody2D _rigidbody2D;

    void Start() {
        Mesh mesh = CreateMesh(size.x, size.y);
        this.transform.rotation = Quaternion.LookRotation(groundGenerator.lookRotation);
        var meshFilter = this.gameObject.AddComponent<MeshFilter>();
        var meshRenderer = this.gameObject.AddComponent<MeshRenderer>();
        meshRenderer.material = groundGenerator.material;
        _rigidbody2D = this.gameObject.AddComponent<Rigidbody2D>();
        _rigidbody2D.mass = 1;
        _rigidbody2D.isKinematic = true;

        var boxCollider2D = this.gameObject.AddComponent<BoxCollider2D>();
        boxCollider2D.size = size;
        meshFilter.mesh = mesh;

        this.transform.position = new Vector3(startPos.x, startPos.y, 0);
        prevPos = this.transform.position;
    }

    void Update() {
        if ((endPos - this.transform.position.ToVector2()).magnitude < 0.01) {
            this._rigidbody2D.velocity = Vector2.zero;
            return;
        }

        var diff = endPos - this.transform.position.ToVector2();

        var diff2 = endPos - prevPos;
        if (Vector2.Dot(diff2, diff) <= 0) {
            this.transform.position = new Vector3(endPos.x, endPos.y, 0);
            this._rigidbody2D.velocity = Vector2.zero;
        } else {
            if (diff.magnitude < groundGenerator.motionThreshold) {
                this._rigidbody2D.velocity = groundGenerator.motionMaxSpeed * Vector2.Lerp(diff / groundGenerator.motionThreshold, diff.normalized, groundGenerator.motionAbruptness);
            } else {
                this._rigidbody2D.velocity = diff / diff.magnitude * groundGenerator.motionMaxSpeed;
            }
        }

        prevPos = this.transform.position;
    }

    public void Init(Vector2 pos, Vector2 size) {
        this.startPos = pos;
        this.endPos = pos;
        this.size = size;
    }

    public void SetEndHeight(float height) {
        this.endPos.y = height;
    }

    public void ChangeEndHeight(float height) {
        this.endPos.y += height;
    }

    Mesh CreateMesh(float width, float height) {
        Mesh m = new Mesh();
        m.name = "ScriptedMesh";
        m.vertices = new Vector3[] {
            new Vector3(-width/2, -height/2, 0.01f),
            new Vector3(width/2, -height/2, 0.01f),
            new Vector3(width/2, height/2, 0.01f),
            new Vector3(-width/2, height/2, 0.01f)
        };
        m.uv = new Vector2[] {
            new Vector2 (0, 0),
            new Vector2 (0, 1),
            new Vector2(1, 1),
            new Vector2 (1, 0)
        };
        m.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
        m.RecalculateNormals();

        return m;
    }
}
