﻿using UnityEngine;
using System.Collections;

public class CheckCollision : MonoBehaviour {

    public BoxCollider2D collider1;
    public BoxCollider2D collider2;
	
	// Update is called once per frame
	void Update () {
        Debug.Log(collider1.bounds.Intersects(collider2.bounds));
	}
}
