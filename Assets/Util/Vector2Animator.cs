﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class Vector2Animator {
    public Vector2 prevPos;
    public float motionThreshold;
    public float motionMaxSpeed;
    public float motionAbruptness;

    public Vector2 Get(ref Vector2 v1, ref Vector2 v2, float dt) {
        var diff = v2 - v1;
        ////if (diff.magnitude < 0.00001) {
        ////    prevPos = v1;
        ////    return v2;
        ////}

        ////var diffPrev = v2 - prevPos;
        ////if (Vector2.Dot(diffPrev, diff) <= 0) {
        ////    prevPos = v1;
        ////    return v2;
        ////}

        Vector2 speed;
        if (diff.magnitude < motionThreshold) {
            speed = motionMaxSpeed * Vector2.Lerp(diff / motionThreshold, diff.normalized, motionAbruptness);
        } else {
            speed = diff.normalized * motionMaxSpeed;
        }

        return v1 + dt * speed;
    }
}