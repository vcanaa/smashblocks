﻿//using UnityEngine;

//class PositionAnimator : MonoBehaviour {
//    public Transform obj;
//    public Vector2 prevPos;
//    public float motionThreshold;
//    public float motionMaxSpeed;
//    public float motionAbruptness;

//    public Vector2 GetVel(Vector2 endPos) {
//        Vector2 result;
//        if ((endPos - obj.position.ToVector2()).magnitude < 0.01) {
//            result = Vector2.zero;
//        } else {
//            var diff = endPos - obj.position.ToVector2();

//            var diff2 = endPos - prevPos;
//            if (Vector2.Dot(diff2, diff) <= 0) {
//                obj.position = new Vector3(endPos.x, endPos.y, 0);
//                result = Vector2.zero;
//            } else {
//                if (diff.magnitude < motionThreshold) {
//                    result = motionMaxSpeed * Vector2.Lerp(diff / motionThreshold, diff.normalized, motionAbruptness);
//                } else {
//                    result = diff / diff.magnitude * motionMaxSpeed;
//                }
//            }
//        }

//        prevPos = obj.position;
//        return result;
//    }
//}