﻿using System.Collections;
using UnityEngine;

public class TimeToLive : MonoBehaviour {
    public float timeToLive;

    void Start() {
        StartCoroutine(Die());
    }

    IEnumerator Die() {
        yield return new WaitForSeconds(timeToLive);
        Entity entity = this.gameObject.GetComponent<Entity>();
        if (entity != null) {
            entity.DestroyGameObject();
        } else {
            Destroy(this.gameObject);
        }
    }
}
