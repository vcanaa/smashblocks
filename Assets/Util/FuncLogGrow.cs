﻿using System;

[Serializable]
public class FuncLogGrow {
    public float amp;
    public float omega;

    public float V(float x) {
        return (float)Math.Log(x * omega + 1) * amp;
    }
}