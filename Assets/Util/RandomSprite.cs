﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class RandomSprite : MonoBehaviour {
    public List<Sprite> sprites;

    void Awake() {
        var sprite = this.GetComponent<SpriteRenderer>();
        sprite.sprite = sprites[(int)UnityEngine.Random.Range(0, sprites.Count)];
    }
}
