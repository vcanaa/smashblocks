﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MonoBehaviour2D : MonoBehaviour {
    public Vector2 pos2d {
        get {
            return new Vector2(transform.position.x, transform.position.y);
        }
        set {
            this.transform.position = new Vector3(value.x, value.y, this.transform.position.z);
        }
    }
}