﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
public class FuncSin {
    public float omega;
    public float amp;
    public float phase;

    public float V(float x) {
        return (float)Math.Sin(x * omega + phase) * amp;
    }
}
