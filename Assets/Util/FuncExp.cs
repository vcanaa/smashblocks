﻿using System;

[Serializable]
public class FuncExp {
    public float omega;
    public float fzero;
    public float finfinite;

    public float V(float x) {
        return (float)Math.Exp(x * omega) * (fzero - finfinite) + finfinite;
    }
}