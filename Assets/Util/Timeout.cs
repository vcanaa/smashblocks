﻿using System;
using UnityEngine;

[Serializable]
public class Timeout {
    public float remaining;
    public float time { get { return total - remaining; } }
    public float total;
    public bool on;

    public Action callback;
    public Action cancelCallback;

    public float timePortion { get { return time / total; } }

    public bool IsOver() {
        return !on;
    }

    public void Update(float dt) {
        if (on) {
            remaining -= dt;
            if (remaining <= 0) {
                on = false;
                if (callback != null) {
                    callback();
                }
            }
        }
    }

    public void Cancel() {
        on = false;
        remaining = 0;
        if (cancelCallback != null) {
            cancelCallback();
        }
    }

    public void Start(float total) {
        this.total = total;
        this.remaining = total;
        this.on = true;
    }

    public void Start() {
        this.remaining = total;
        this.on = true;
    }
}