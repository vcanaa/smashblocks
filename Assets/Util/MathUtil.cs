﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class MathUtil {
    public static float RoundCloser(float value, float unit) {
        return Closer(value, (float)Math.Floor(value), (float)Math.Ceiling(value));
    }

    public static float Closer(float value, float a, float b) {
        if (Math.Abs(value - a) < Math.Abs(value - b)) {
            return a;
        }

        return b;
    }
}
