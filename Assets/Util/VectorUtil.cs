﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class VectorUtil {
    public static Vector2 ToVector2(this Vector3 v) {
        return new Vector2(v.x, v.y);
    }

    public static Vector3 ToVector3(this Vector2 v, float z = 0) {
        return new Vector3(v.x, v.y, z);
    }

    public static Vector3 Sum(this Vector3 v1, ref Vector2 v2) {
        return new Vector3(v1.x + v2.x, v1.y + v2.y, v1.z);
    }

    public static Vector2 Reflect(this Vector2 v, Vector2 normal) {
        normal.Normalize();
        Vector2 surface = normal.Normal();
        float x = Vector2.Dot(v, surface);
        return 2 * x * surface - v;
    }

    public static Vector2 Normal(this Vector2 v) {
        return new Vector2(v.y, -v.x);
    }

    public static float Cross(Vector2 v1, Vector2 v2) {
        return v1.x * v2.y - v2.x * v1.y;
    }
}