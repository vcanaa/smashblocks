﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;


//public class CameraControl : MonoBehaviour {

//    public List<Transform> objectsToTrack;
//    public float minSize;
//    public float maxSize;

//    public Vector2 llCorner;
//    public Vector2 urCorner;

//    PositionAnimator animator;

//    // Use this for initialization
//    void Start () {
//        animator = this.GetComponent<PositionAnimator>();
//        animator.obj = this.transform;
//    }
	
//    // Update is called once per frame
//    void Update () {
//        Vector2 endPos = Vector2.zero;
//        foreach (var t in objectsToTrack) {
//            endPos.x += t.position.x;
//            endPos.y += t.position.y;
//        }

//        endPos /= objectsToTrack.Count;
//        Vector2 speed = animator.GetVel(endPos);

//        //aDebug.Log("speed " + speed);

//        this.transform.position += new Vector3(speed.x, speed.y, 0) * Time.deltaTime;
//    }
//}
