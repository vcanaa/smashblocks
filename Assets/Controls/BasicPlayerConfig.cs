﻿using UnityEngine;
using System.Collections;

public class BasicPlayerConfig : MonoBehaviour {

    // Movement
    public float jumpIntensity;
    public float deadzone;
    public float runIntensity;
    public float maxRunSpeed;
    public float runDecrease;
    public float maxFallSpeed;
    public float gravity;
    public float airControl;
    public float airLimit;
    public float airDecrease;

    public float standLimit;
    public float stepLimit;
    public float stepPortion;
    public float fullStepLimit;
    public float horizontalNormalLimit;

    public float wallLimit;
    public float maxWallFallSpeed;
    public Vector2 onWallJumpSpeed;

    public float holdingWallCooldownLimit;
    public float standingCooldownLimit;

    public float throwBoulderSpeed;

    public float bounceSpeed;

    public float dash;
    public float dashTime;
    public float redashTime;
    public float dashEndSpeed;
    public float throwThrashold;
    public float blockGravity;

    public float hitTime;
    public float hitIntensity;
    public float blockSpeedHit;
    public float hitIncrease;
    public float hitMultiplierMax;

    public float crouchLimit;

    public static BasicPlayerConfig get;

    void Awake() {
        get = this;
    }

    public float dashSpeedHit;
}
