﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ControllerWrapper : ControllerWrapperBase {
    public Dictionary<string, float> timePressed;

    public bool isKeyboard;

    public Dictionary<string, KeyCode> keyboardMap;

    public Dictionary<string, string> controllerMap;


    ControllerWrapper() {
        timePressed = new Dictionary<string, float>();
        keyboardMap = new Dictionary<string, KeyCode>{
                {"A", KeyCode.C},
                {"B", KeyCode.X},
                {"X", KeyCode.X},
                {"Y", KeyCode.S},
                {"L1", KeyCode.D},
                {"R1", KeyCode.Z},
                {"START", KeyCode.S},
                {"left", KeyCode.J},
                {"right", KeyCode.L},
                {"up", KeyCode.I},
                {"down", KeyCode.K},
            };

        controllerMap = new Dictionary<string, string> {
                {"A", "0"},
                {"B", "1"},
                {"X", "2"},
                {"Y", "3"},
                {"L1", "4"},
                {"R1", "5"},
                {"START", "7"},
            };

        foreach (var key in controllerMap.Keys) {
            timePressed[key] = float.MaxValue / 2;
        }
    }

    public void Awake() {

        enabled = true;
    }

    public override bool GetKeyDown(string key, float timeLimit) {
        if (!enabled) return false;

        if (isKeyboard) {
            return Input.GetKeyDown(keyboardMap[key]);
        }

        if (Input.GetKeyDown(Bname(key)) || Input.GetKey(Bname(key)) && timePressed[key] <= timeLimit) {
            return true;
        }

        return false;
    }

    public bool GetKeyDown(string key) {
        if (!enabled) return false;

        if (isKeyboard) {
            return Input.GetKeyDown(keyboardMap[key]);
        }

        return Input.GetKeyDown(Bname(key));
    }

    public override bool GetKeyUp(string key) {
        if (!enabled) return false;

        if (isKeyboard) {
            return Input.GetKeyUp(keyboardMap[key]);
        }

        return Input.GetKeyUp(Bname(key));
    }

    public override bool GetKey(string key) {
        if (!enabled) return false;

        if (isKeyboard) {
            return Input.GetKey(keyboardMap[key]);
        }

        return Input.GetKey(Bname(key));
    }

    private string Bname(string key) { return string.Concat("joystick ", player + 1, " button ", controllerMap[key]); }

    public override Vector2 GetDir1() {
        if (!enabled) return Vector2.zero;

        Vector2 dir = new Vector2();
        if (isKeyboard) {
            if (Input.GetKey(keyboardMap["right"])) {
                dir.x = 1;
            }

            if (Input.GetKey(keyboardMap["left"])) {
                dir.x = -1;
            }

            if (Input.GetKey(keyboardMap["up"])) {
                dir.y = 1;
            }

            if (Input.GetKey(keyboardMap["down"])) {
                dir.y = -1;
            }
        } else {
            dir.x = Input.GetAxis(H1name);
            dir.y = Input.GetAxis(V1name);
        }

        return dir;
    }

    private string H1name {
        get { return string.Concat("p", player + 1, "AH1"); }
    }

    private string V1name {
        get { return string.Concat("p", player + 1, "AV1"); }
    }

    public override void UpdateKeySet(float dt) {
        foreach (var key in controllerMap.Keys) {
            if (GetKeyDown(key)) {
                timePressed[key] = dt;
            } else if (GetKey(key)) {
                timePressed[key] += dt;
            } else {
                timePressed[key] = float.MaxValue / 2;
            }
        }
    }
}