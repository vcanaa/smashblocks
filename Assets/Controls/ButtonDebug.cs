﻿using UnityEngine;
using System.Collections;

public class ButtonDebug : MonoBehaviour {
    public bool button00;
    public bool button01;
    public bool button02;
    public bool button03;
    public bool button04;
    public bool button05;
    public bool button06;
    public bool button07;
    public bool button08;
    public bool button09;
    public bool button10;
    public bool button11;
    public bool button12;
    public bool button13;
    public bool button14;
    public bool button15;
    public bool button16;
    public bool button17;
    public bool button18;
    public bool button19;
    public float DH;
    public float DV;
    public float AH1;
    public float AV1;
    public float AH2;
    public float AV2;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		button00 = Input.GetKey (KeyCode.Joystick1Button0);
		button01 = Input.GetKey (KeyCode.JoystickButton1);
		button02 = Input.GetKey (KeyCode.JoystickButton2);
		button03 = Input.GetKey (KeyCode.JoystickButton3);
		button04 = Input.GetKey (KeyCode.JoystickButton4);
		button05 = Input.GetKey (KeyCode.JoystickButton5);
		button06 = Input.GetKey (KeyCode.JoystickButton6);
		button07 = Input.GetKey (KeyCode.JoystickButton7);
		button08 = Input.GetKey (KeyCode.JoystickButton8);
		button09 = Input.GetKey (KeyCode.JoystickButton9);
		button10 = Input.GetKey (KeyCode.JoystickButton10);
		button11 = Input.GetKey (KeyCode.JoystickButton11);
		button12 = Input.GetKey (KeyCode.JoystickButton12);
		button13 = Input.GetKey (KeyCode.JoystickButton13);
		button14 = Input.GetKey (KeyCode.JoystickButton14);
		button15 = Input.GetKey (KeyCode.JoystickButton15);
		button16 = Input.GetKey (KeyCode.JoystickButton16);
		button17 = Input.GetKey (KeyCode.JoystickButton17);
		button18 = Input.GetKey (KeyCode.JoystickButton18);
        button19 = Input.GetKey (KeyCode.JoystickButton19);
        DH = Input.GetAxis("p1DH");
        DV = Input.GetAxis("p1DV");
        AH1 = Input.GetAxis("p1AH1");
        AV1 = Input.GetAxis("p1AV1");
        AH2 = Input.GetAxis("p1AH2");
        AV2 = Input.GetAxis("p1AV2");
	}
}
