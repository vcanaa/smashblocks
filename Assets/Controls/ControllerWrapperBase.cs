﻿using System.Collections.Generic;
using UnityEngine;

public abstract class ControllerWrapperBase : MonoBehaviour {
    protected Dictionary<string, bool> prevKeys = new Dictionary<string, bool>();
    protected Dictionary<string, bool> keys = new Dictionary<string, bool>();

    protected ControllerWrapperBase() {
        prevKeys = new Dictionary<string, bool>();
        Copy(keys, prevKeys);
    }

    public int player;

    public abstract bool GetKeyDown(string key, float timeLimit);

    public abstract bool GetKeyUp(string key);

    public abstract bool GetKey(string key);

    public abstract Vector2 GetDir1();

    public abstract void UpdateKeySet(float dt);

    protected void Copy(Dictionary<string, bool> from, Dictionary<string, bool> to) {
        foreach (var i in from) {
            to[i.Key] = i.Value;
        }
    }
}
